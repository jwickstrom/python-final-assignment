import json
import os

def read_cart():
    
    if os.path.getsize("data/shopping_cart.json") != 0: # Checking whether JSON-file is empty or not.  
        try:
            with open("data/shopping_cart.json", "r") as cart_out:
                return json.load(cart_out)
        except:
            print("file error!!")

    else:
        print("Sorry, but your cart carries no items.")
        return 0

def write_to_invoice(full):
    pass
#     with open('data/invoice.txt', 'w') as invoice_out:
#         invoice_out.write()


def main():
    print("Option is not yet available. Won't do anything.")
    full_cart = read_cart()
    write_to_invoice(full_cart)
        
if __name__ == '__main__':
    main()