import os
import json

# import Coupon

from datetime import datetime # To insert the current time to log

def read_cart():
    
    if os.path.getsize("data/shopping_cart.json") != 0: # Checking whether JSON-file is empty or not.  
        try:
            with open("data/shopping_cart.json", "r") as cart_out:
                return json.load(cart_out)
        except:
            print("file error!!")

    else:
        print("Sorry, but your cart carries no items.")
        return 0
        
def print_cart(full_cart):
    '''Method with two purposes. Counts the instances of coupons in the cart. 
    Also pretty printing the full cart with corresponding total cost and discounts. 
    '''
    
    disc = 0 
    for k, v in full_cart.items(): # Count the instances of coupons in the cart.
        if k == "Coupon":
            disc = v["order_size"]
    
    
    total_total_cost = 0

    max_key_len = max([len(x) for x in full_cart.keys()]) + 1 # Not sure if I need this!!
    max_val_len = max([len(str(x)) for x in full_cart.values()]) + 1
    
    print(f"| "+"-"*(max_key_len+max_val_len)+" |")
    
    for k, v in full_cart.items():
        
        form_len = len(k)
        total_len = form_len + max_key_len+ max_val_len
        total_item_cost = v["cost"] * v["order_size"]
        total_total_cost += total_item_cost
        print(f"{k} {format(v['cost'], '.2f'):>{22-form_len}} SEK x {v['order_size']} {format(total_item_cost, '.2f'):>11} SEK")
        
    print(f"| "+"-"*(max_key_len+max_val_len)+" |")
    print(f"Total: {format(total_total_cost, '.2f')} SEK." if disc == 0 else f"Total: {format((total_total_cost*(1-(disc*5)/100)), '.2f')} SEK. With {disc*5}% discount.")
        
    return disc # Only returns the number of discounts, everything else is printed within the method
    
def get_credit_info():    
    ''''''
    return_value = None 
    quit_by_q = False
    full_credit_card_details = [] # Declaring a list where all credit details will be stored. 
    
    while (return_value is None and not quit_by_q): 
        
        credit_input = input("Please input your credit card information! Else, press q.")
        credit_input = credit_input.replace(" ", "") # Erasing any blank-spaces. Should add up to 16 w/o

        if credit_input == "q" or credit_input == "Q": # Press q and program breaks to main menu.
            quit_by_q = True
            break
            
        elif credit_input.isdecimal(): # If only numbers in the inputting string
            if len(credit_input) == 16:
                full_credit_card_details.append(credit_input)
                
                while (return_value is None and not quit_by_q):
                    credit_surname_input = input("Accepted number. Please insert surname.")

                    if credit_surname_input == "q" or credit_surname_input == "Q": # Press q and program breaks to main menu.
                        quit_by_q = True
                        break
                    elif credit_surname_input.isalpha():
                        full_credit_card_details.append(credit_surname_input.lower().capitalize())
                        
                        # Last step, CVV. 3 digits exactly. 
                        while (return_value is None and not quit_by_q):
                            credit_cvv_input = input("Accepted surname. Please insert CVV.")
                            
                            if credit_cvv_input == "q" or credit_cvv_input == "Q": # Press q and program breaks to main menu.
                                quit_by_q = True
                                break
                            elif credit_cvv_input.isdecimal() and len(credit_cvv_input) == 3:
                                print("PAYMENT SUCCESS!")
                                full_credit_card_details.append(credit_cvv_input)
                                print(full_credit_card_details) # TODO, space inbetween credit card number?
                                return_value = full_credit_card_details
                            else:
                                print("Not correct format of CVV. 3 numbers, 0-9 please.")
                    else:
                        print("Only letters from a-z")
                        #credit_surname_input = None
            else:
                print("Not correct format, 16 numbers are needed")
                
        else:
            print("No valid info, try again.")
    
    return return_value

def empty_cart():
    try:
        with open("data/shopping_cart.json", "r+") as cart_to_be_emptied:
            cart_to_be_emptied.truncate(0)
    except:
        print("File not found")
        
def write_to_log(credit_info_list):
    '''Method that writes information to log. Both credit card information and the time/date of the transaction. '''
    now = datetime.now() # Now is now
    
    if len(credit_info_list) == 3 and type(credit_info_list) == type([]) and credit_info_list is not None: # If statements are correct, read and append to log.
        try:
            with open("data/transactions.log", "r") as log_in, open("data/transactions.log", "a") as log_out:
                log_out.write(f"\n{credit_info_list[0]}, {credit_info_list[1]}, {credit_info_list[2]} - {now}")

        except:
            print("File error when logging")

def store_premium_status(full_cart):
    '''Method that stores whether the user has payed for Premium or not. 
    This method has the sole purpose however of making the user "Premium"'''
    for k, v in full_cart.items():
         if k == "Premium Pass" and v["order_size"] >0:
            print("The premium pass was added to your account! NO MORE ADS!")
            
            data = {k: {"valid": 1}} 
            
            try:
                with open("data/user_info.json", "w") as cart_out:
                    json.dump(data, cart_out)
            except:
                print("Error ?!!")
                        
def main():
    '''Begin CheckoutCart.main with printing the full shopping-cart.
    Also keeps track of number of current coupons.
    Then prompts the user for the credit card information. 
    If all is correct. Finally, write some info to the log & store the Premium instance (if in cart at the time) & Fully empty the cart!'''
    full_cart = read_cart()
    print("") # For some spacing
    
    if full_cart != 0:
        print("Below is your full cart and total cost.")
        instances_of_coupons = print_cart(full_cart) 
        
        credit_info_list = get_credit_info()
        if credit_info_list is not None:
            write_to_log(credit_info_list)
            store_premium_status(full_cart)
            empty_cart()
    
if __name__ == '__main__':
    main()