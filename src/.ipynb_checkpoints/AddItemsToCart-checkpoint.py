import ViewCatalog # To fetch the current inventory.
import json # json for handling the .json files.
import os # os to quickly comprehend file size. 

def purchase_input(inventory):
    '''A method that prompts the user for purchasing intention information. Every input will loop until a valid command is given.
    At any point, quit the program with "q" '''
    
    quit_by_q = False # A bool to check for q-clicks
    return_value = None 
#    print(inventory)

    while (return_value is None and not quit_by_q): # Unless a product is found, loop. 
        print("") # For some spacing
        not_found = True # A checker for not finding the product, making it print once instead on every for loop.
        
        id_input = input("Please enter Product ID. Insert q for main menu")
        
        if id_input == "q" or id_input == "Q": 
            quit_by_q = True
            break
            
        elif id_input.isalnum(): # We only accept letters and numbers. 
            
            for k, v in inventory.items(): 
                if id_input.upper() == v['product_id']: 
                    not_found = False
                    if v['instock']>0:
                        print(f'''{k} is available, it costs {v['cost']} SEK. {v['instock']} is currently in stock!''') # If it is in stock, print the product info
                        
                        correct_order = False
                        while correct_order == False:
                            print("") # For some spacing
                            order_size = input(f"How many of the {k} to purchase? Insert 0 for main menu." )
                            if order_size == "q":
                                break
                            try: # A try to convert the string-input into integer.
                                order_size = int(order_size)
                                
                                if order_size == 0:
                                    break
                                elif order_size < 0:
                                    print("Only positive numbers please. Try again")
                                    correct_order = False
                                elif order_size > v['instock']:
                                    print("Not that many in stock. Try again.")
                                elif order_size > 0 and order_size <= v['instock']:
                                    correct_order = True
                                    final_call = input(f"Please confirm an order of {order_size} {k} by pressing 'y'") #Extra print for user to confirm the order.
                                    print("") # Some spacing
                                    if final_call == 'y':
                                        
                                        #Making sure the invent is updated, also the shopping cart.
                                        v['instock'] -= order_size

                                        remove_from_invent(inventory) # Calling method to erase items from inventory.json
                                        add_to_cart(k, v['cost'], order_size) # Calling method to add users' order to cart.
                                                                                
                                        return_value = [id_input.upper(), order_size] # Even if user enters low case, we return it as upper case. Also order size.
                                    else:
                                        print("Alright, let's try again.")
                                        correct_order = False
                            except ValueError:
                                print("Not a number. try using the numbers: 1-9")
                        break
                    else:
                        print("The item is out of stock. Try another ID please.")
                else:
                    not_found = True
            if not_found:
                print("The product ID was not found. Try another ID please.")
            else:
                pass
        else:
            print(f"Don't use special characters! Only a-z and 0-9, please.")
    return return_value
    
def remove_from_invent(invent):
    '''Method that writes an updates inventory file. Because when adding to cart, the inventory is also decreased by likewise many. '''
    try:
        with open("data/inventory.json", "w") as invent_out:
            json.dump(invent, invent_out)
            print("Wise choice sir. The items have been added to your shopping cart.") # TODO Put in add_to_Cart !!!
    except:
        print("No file found")

def add_to_cart(name, cost, size):
    '''Method that puts the items in question into the shopping_cart.json file. By the standard below.'''
    data = {name: {"cost": cost,
                        "order_size": size }
                       }
    
    if os.path.getsize("data/shopping_cart.json") == 0: # Checking whether JSON-file is empty or not. If it is empty, we only write. 
        try:
            with open("data/shopping_cart.json", "w") as cart_out:
                                json.dump(data, cart_out)
        except:
            print("Error in first one!!")
    else:
        
        try:
            with open("data/shopping_cart.json", "r") as cart_in: # Read the file and load the content to a dict. 
                current_cart = json.load(cart_in)  
                no_such_item = True # If file is later found, this turns False

                for k,v in current_cart.items():
                    if name == k: # If this True, change amount rather than append new.
                        v["order_size"] += size

                        no_such_item = False
                    else:
                        pass
                if no_such_item: # If no item was found found. We want to append at the end of the file. By updating the cart and then dumping to json.file.
                    try:
                        with open("data/shopping_cart.json", "w") as cart_out:
                            current_cart.update(data)
                            cart_out.seek(0) # Seek to reset the file pointer to position 0
                            json.dump(current_cart, cart_out)
                            print("Added new item to cart")
                    except:
                        print("Could not add new item.")
                else:
                    try:
                        with open("data/shopping_cart.json", "w") as cart_out:
                            print("Shopping cart was updated corrrectly!")
                            json.dump(current_cart, cart_out)
                    except:
                        print("Could not add to existing item.")
        except:
            print("Error in fily-handling!!")
        
def main():
    
    inventory_func = ViewCatalog.open_invent # Instantly call this to fetch the current inventory. From file. It gets stored as a function-name
    inventory = inventory_func() # Call the function, store it and pass it to purchase_input.
    
    add_info = purchase_input(inventory)
    #add_to_cart(add_info)

       
if __name__ == '__main__':
    main()