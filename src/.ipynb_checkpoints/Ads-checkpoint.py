import os
import json

import UserInfo

def add_pass_to_cart():
    '''If user has stated its interest in th Premium Pass
    This method fetches the shopping cart from file and appending an extra item: Premium Pass.'''
    
    already_in_cart = False
    name = "Premium Pass"
    data = {name: {"cost": 150, 
                        "order_size": 1 }
                       } # The data is appended in this format. 
    
    if os.path.getsize("data/shopping_cart.json") == 0: # Checking whether JSON-file is empty or not.  
        try:
            with open("data/shopping_cart.json", "w") as cart_out:
                json.dump(data, cart_out)
                print("Good choice! The ads will be gone once you pay!")
        except:
            print("Error in first one!!")

    else:
        try:
            with open("data/shopping_cart.json", "r") as cart_in: # If there is a file with items
                current_cart = json.load(cart_in)  

                no_such_item = True # Predeclare that an item has not yet been found. 

                for k,v in current_cart.items():
                    if name == k: # If this True, a Premium pass is already in the cart.
                        if v["order_size"] > 0:
                            print("Already a Premium Pass in the shopping cart!")
                            already_in_cart = True
                            break
                    else:
                        pass
                    
                if no_such_item and not already_in_cart: # If no similair item in cart, update the cart with the premium-data-dict above. 
                    try:
                        with open("data/shopping_cart.json", "w") as cart_out:
                            current_cart.update(data)
                            cart_out.seek(0) # Seek to reset the file pointer to position 0
                            json.dump(current_cart, cart_out)
                            print("Good choice! A Premium Pass has been added to your cart!")
                    except:
                        print("Could not add new item.")
                else:
                    try:
                        with open("data/shopping_cart.json", "w") as cart_out:
                            json.dump(current_cart, cart_out)
                    except:
                        print("Could not add to existing item.")
        except:
            print("Error in fily-handling!!")
        
def ask_for_ad_free():
    '''Method that prompts the user if it wants a Premium pass, if yes. start method add_pass_to_cart().'''
    
    quit_by_q = False # A bool to check for q-clicks
    return_value = None 

    while (return_value is None and not quit_by_q) : # Unless a product is found, loop. 
        print("") # For some spacing
        not_found = True # A checker for not finding the product, making it print once instead on every for loop.

        answer_input = input("Do you want to purchase Pemium Pass for 150 SEK? (y/n)")

        if answer_input == "y":
            add_pass_to_cart()
            break

        elif answer_input == "n":
            print("Maybe next time!")
            quit_by_q = True
        else:
            pass
            
def main():
    '''Main objective of this main is to decide whether a user should see ads or not. While also letting them putting in the shopping cart for later purchasing. 
    The main only fetches the UserInfo file to know if it should print ads and prompt for it when opening option 5 in pyukea.
    '''
    is_premium = UserInfo.main
    is_premium = is_premium()
    
    if is_premium != 1:
        ask_for_ad_free()
    else:
        print("Thank you for already having the Premium Pass!")

if __name__ == '__main__':
    main()