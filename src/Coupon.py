import json # To use json files
import os # To check file size quickly

import CheckoutCart

def get_coupon_list(user_c):
    '''Method that aims to compare the users input with the coupon file that have in directory.
    When writing the coupon file, for every line we rewrite it to the file yet again. EXCEPT for when we have a match. 
    If a match, discount_activated turns to True, which is being returned. '''
    list_ = ""
    discount_activated = False
    
    try:
        with open ("data/coupon_codes.txt", "r") as coupon_list_in:
                    
            for line in coupon_list_in.readlines():
                if line[:len(line)-1] == user_c or line == user_c:
                    discount_activated = True
                else:
                    list_ += (line)
            try: 
                with open('data/coupon_codes.txt', 'w') as coupon_list_out:
                    for items in list_:
                        coupon_list_out.write(items)
            except:
                print("Nothing to write to")
    except:
        print("Problem finding coupon list.")
    

    return discount_activated 
         
def enter_coupon():
    '''Method that takes user input and firstly validates its format. If the format is accepted, pass the input to '''
    quit_by_q = False # A bool to check for q-clicks
    return_value = None 

    while (return_value is None and not quit_by_q):  
        print("") # For some spacing
        not_found = False # A checker for not finding the coupon, making it print once instead on every for loop.
        
        coupon_input = input("Please enter Coupon Code. Insert q for main menu")
        
        if coupon_input == "q" or coupon_input == "Q":
            quit_by_q = True
            break
        elif coupon_input.isalnum(): # We only accept letters and numbers. 
            if coupon_input[:10].isdecimal() and coupon_input[14:].isalpha() and len(coupon_input) == len("581401701249BWZBTZKG"): # I noticed some codes were slightly different formats. I increased the width of the checker slightly. Correct lenght is important tho. 
                
                
                return_value = get_coupon_list(coupon_input)
                
            else:
                print("Wrong coupon format! Try again.")
        else:
            print("Wrong coupon format! Try again.")
    return return_value
    
def add_coupon_to_cart(coup_id):
    
    name = "Coupon"
    data = {name: {"cost": 0,
                        "order_size": 1 }
                       }
    
    if os.path.getsize("data/shopping_cart.json") == 0: # Checking whether JSON-file is empty or not.  
        try:
            with open("data/shopping_cart.json", "w") as cart_out:
                                json.dump(data, cart_out)
        except:
            print("Error in first one!!")

    else:
         # 3 cases
        try:
            with open("data/shopping_cart.json", "r") as cart_in:
                current_cart = json.load(cart_in)  
                #print("Cart:", current_cart)

                no_such_item = True

                for k,v in current_cart.items():
                    if name == k: 
                        if v["order_size"]<3: # If this True, change amount rather than append new.
                            v["order_size"] += 1
                            no_such_item = False
                            print("Added yet another discount to cart!")
                        else:
                            print("Only 3 discounts simultaneously!")
                    else:
                        pass

                if no_such_item:
                    try:
                        with open("data/shopping_cart.json", "w") as cart_out:
                            current_cart.update(data)
                            cart_out.seek(0) # Seek to reset the file pointer to position 0
                            json.dump(current_cart, cart_out)
                            print("Added discount to cart!")
                    except:
                        print("Could not add new item.")
                else:
                    try:
                        with open("data/shopping_cart.json", "w") as cart_out:
                            json.dump(current_cart, cart_out)
                    except:
                        print("Could not add to existing item.")

        except:
            print("Error in fily-handling!!")   
    
def main():
    '''Method that lets user enter a coupon, then compare to list of correct ones. If correct match, adding the coupon to cart'''
    validate_coup = False
    get_user_input = enter_coupon()

    if get_user_input:
        print("Coupon matches, you now have discount on next purchase!")
        add_coupon_to_cart(get_user_input)

    else:
        print("Try different code.")
    
if __name__ == '__main__':
    main()