import json

def get_premium_info():
    '''Method that has the sole purpose of making a user Premium, by storing a value of 1 in a json file.'''
    return_value = 0
    try:
        with open("data/user_info.json", "r") as user_out:
            info_ = json.load(user_out)
            for k, v in info_.items():
                # print(k,v)
                if k == "Premium Pass" and v["valid"] == 1:
                    return_value = 1
    except:
        print("Error ?!!")
    
    return return_value

def main():

    return get_premium_info()
 
if __name__ == '__main__':
    main()